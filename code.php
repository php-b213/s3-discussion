<?php
	//Objects as Variables
		$buildingObj = (object)[
			'name' => 'Caswynn Building',
			'floors' => 8,
			'address' => (object)[
				'barangay' => 'Sacred Heart',
				'city' => 'Quezon City',
				'country' => 'Philippines'
			]
		];


	//Objects from Classes
		//Writing convention of class name: PASCAL CASE
		class Building {
			public $name;
			public $floors;
			public $address;

			/* public function __(double underscore)construct()*/
			/* we use 'this' keyword to state that this variable is a property of the class 'Building'
			*/

		//Function
			public function __construct($name, $floors, $address){
				$this->name = $name;
				$this->floors = $floors;
				$this->address = $address;
			}

		//Method of the function
			public function printName(){
				return "The name of the building is $this->name";
			}
		}

		//Instantiation 
		$building = new Building('Caswynn Building', 8, 'Timog Avenue, Quezon city, Philippines');


//[SECTION] Inheritance
	//the Building parent class properties will be inherited by child class "Condominium".
		class Condominium extends Building {
			//POLYMORPHISM -> methods inherited can be overriden.
			public function printName1(){
				return "The name of the condominium is $this->name";
			}
		}

		//Instantiation
		$condominium = new Condominium ('Enzo Condo', 5, 'Buendia Avenue, Makati City, Philippines');


//[SECTION] Polymorphism




/*
Additional Notes

	Object-Oriented Programming Approach:
		This approach focuses more on how data is structured within the program and how is it going to be used.

	Classes and Objects:
		- Classes are blueprints that define values and behaviors.
		- Objects are the implementation of these classes. 
		- Objects created from a class are referred to as instances of a class.

	The Four Pillars of OOP:
		- Inheritance. The derived classes are allowed to inherit variables and methods from a specified base class.

		- Abstraction. Only the object’s features are visible but the actual implementation are hidden.

		- Encapsulation. Also known as data binding, it dictates that data must not be directly accessible to users but through a public function called property.

		- Polymorphism. Methods inherited by a derived class can be overridden to have a behavior different from the method of base class.
*/

?>


