<?php require_once "./code.php" ?>
<!DOCTYPE html>
<html>
<head>
	<meta charset="utf-8">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<title>S3: Classes, Objects, Inheritance and Polymorphism</title>
</head>
<body>
	<h1>Objects from Variable</h1>
	<p><?php echo $buildingObj->name;?></p>

	<h1>Objects from Classes</h1>
	<p><?php echo var_dump($building)?></p>

	<h1>Inheritance ('Condominium' object)</h1>
	<p><?php echo $condominium->name; ?></p>
	<p><?php echo $condominium->floors; ?></p>
	
	<h1>Polymorphism (Changing the printName Behavior)</h1>
	<p><?php echo $condominium->printName1(); ?></p>


</body>
</html>